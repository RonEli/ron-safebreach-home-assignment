/*
 * Testing the waters using a redis client
 */
var redis = require("redis");
// TODO: use magic number or get from command line the actual port to connect
var client = redis.createClient(6379);

/*
 * Constants
 */

var ASSETS_KEY = "Assets";
/*
 * Initialize client
 */
client.on("error", function(err) {
    console.log("Error " + err);
});


function delete_assets()
{
    client.del(ASSETS_KEY);
}
// This function gets all assets stored in Redis server
//TODO: call with callback function from router
function getAssets(callback) {
    client.smembers(ASSETS_KEY, function (err, reply) {
        var assets = JSON.parse(reply);
        console.log(assets);
    });
}

function addAsset(asset)
{
    client.sadd(ASSETS_KEY, JSON.stringify(asset), redis.print);
}


// Minor test
delete_assets();
addAsset({"test": 123});
console.log(getAssets());
module.exports.getAssets = getAssets;
module.exports.client = client;





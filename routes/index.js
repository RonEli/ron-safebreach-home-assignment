/*
 * This module defines simple asset commands for CNC server
 */

/*
 * Module dependencies.
 */
var express = require('express');
var bodyParser = require("body-parser");

/*
 * Initialize router.
 */

var router = express.Router();
// instruct the ap to use 'bodyParser()' middleware for all routes
router.use(bodyParser.json());

/*
 * Initialize from redis server
 */
var assets_list = [];

router.post('/assets', function(req, res) {

    // TODO: allow more assets to be posted to the CnC
    var credit_details = req.body.CreditDetails;
    if (!credit_details){
        return res.status(400).send({"error": "Missing Credit details"})
    }

    var is_asset_valid = (credit_details.CardNumber && credit_details.IssuingNetwork);
    if (!is_asset_valid)
    {
        return res.status(400).send({"error": "Missing CardNumber or IssuingNetwork"})
    }

    // Add asset
    // TODO: verify same asset isn't inserted more then once
    assets_list.push({"CreditDetails" : credit_details});
    // Update response
    res.json({"NumAssets": assets_list.length});
});

router.get('/assets', function(req, res) {
    res.json(assets_list);
});

router.delete('/assets', function (req, res) {
    res.json(assets_list);
    assets_list = [];
});


module.exports = router;

/*
 * Basic testing for the code, never done unitesting in NodeJS, so I tried it out of curiosity
 * TODO: I noticed my tests aren't truly dependent, should restart server or clean data before every test.
 */

var request = require("supertest");
var app = require('../app');



/*
 * Get assets test
 */
describe('GET /cnc/assets', function () {
    it('respond no assets', function (done) {
        request(app)
            .get("/cnc/assets")
            .set('Accept', 'application/json')
            .expect('Content-Type', "application/json; charset=utf-8")
            .expect(200, [] , done);
    });
});

/*
 * Delete assets test
 * TODO: I don't like the fact that I am only checking delete with Get and Post..
 */
describe('DELETE /cnc/assets', function () {
    it('Add new asset', function (done) {
        request(app)
            .post("/cnc/assets")
            .send({"CreditDetails": {"IssuingNetwork": "American Express", "CardNumber": 375283401434484}})
            .set('Accept', 'application/json')
            .expect('Content-Type', "application/json; charset=utf-8")
            .expect(200, {"NumAssets" : 1}, done);
    });

    it('delete', function (done) {
        request(app)
            .delete("/cnc/assets")
            .expect('Content-Type', "application/json; charset=utf-8")
            .expect(200,   [
                {"CreditDetails": { "CardNumber": 375283401434484,  "IssuingNetwork": "American Express"}}
            ], done); // expected empty list response
    });

    it('Get available assets after delete', function (done) {
        request(app)
            .get("/cnc/assets")
            .set('Accept', 'application/json')
            .expect('Content-Type', "application/json; charset=utf-8")
            .expect(200,[], done);
    });

    it('Add new asset', function (done) {
        request(app)
            .post("/cnc/assets")
            .send({"CreditDetails": {"IssuingNetwork": "American Express", "CardNumber": 375283401434484}})
            .set('Accept', 'application/json')
            .expect('Content-Type', "application/json; charset=utf-8")
            .expect(200, {"NumAssets" : 1}, done);
    });

    it('Add new asset', function (done) {
        request(app)
            .post("/cnc/assets")
            .send({"CreditDetails": {"IssuingNetwork": "Visa", "CardNumber": 774283401654474}})
            .set('Accept', 'application/json')
            .expect('Content-Type', "application/json; charset=utf-8")
            .expect(200, {"NumAssets" : 2}, done);
    });

    it('Get available assets before delete', function (done) {
        request(app)
            .get("/cnc/assets")
            .set('Accept', 'application/json')
            .expect('Content-Type', "application/json; charset=utf-8")
            .expect(200,[
                {"CreditDetails": { "CardNumber": 375283401434484,  "IssuingNetwork": "American Express"}},
                {"CreditDetails": {"IssuingNetwork": "Visa", "CardNumber": 774283401654474}}
            ], done);
    });
    it('Delete multiple assets', function (done) {
        request(app)
            .delete("/cnc/assets")
            .expect('Content-Type', "application/json; charset=utf-8")
            .expect(200,   [
                {"CreditDetails": { "CardNumber": 375283401434484,  "IssuingNetwork": "American Express"}},
                {"CreditDetails": {"IssuingNetwork": "Visa", "CardNumber": 774283401654474}}
            ], done); // expected empty list response
    });

    it('Get available assets after multiple delete', function (done) {
        request(app)
            .get("/cnc/assets")
            .set('Accept', 'application/json')
            .expect('Content-Type', "application/json; charset=utf-8")
            .expect(200,[], done);
    });

});

/*
 * Post Assets Test
 */
describe('POST /cnc/assets', function () {
    it('Post new asset', function (done) {
        request(app)
            .post("/cnc/assets")
            .send({"CreditDetails": {"IssuingNetwork": "American Express", "CardNumber": 375283401434484}})
            .set('Accept', 'application/json')
            .expect('Content-Type', "application/json; charset=utf-8")
            .expect(200, {"NumAssets" : 1} , done);
    });

    it('Get available assets after post', function (done) {
        request(app)
            .get("/cnc/assets")
            .set('Accept', 'application/json')
            .expect('Content-Type', "application/json; charset=utf-8")
            .expect(200,[
                {"CreditDetails": { "CardNumber": 375283401434484,  "IssuingNetwork": "American Express"}}
            ], done);
    });

    it('Post bad asset', function (done) {
        request(app)
            .post("/cnc/assets")
            .send({"BadAsset": {"IssuingNetwork": "American Express", "CardNumber": 375283401434484}})
            .set('Accept', 'application/json')
            .expect('Content-Type', "application/json; charset=utf-8")
            .expect(400, {"error" : "Missing Credit details"} , done);
    });

    it('Post bad Credit details', function (done) {
        request(app)
            .post("/cnc/assets")
            .send({"CreditDetails": {"FakeKey": "American Express"}})
            .set('Accept', 'application/json')
            .expect('Content-Type', "application/json; charset=utf-8")
            .expect(400, {"error": "Missing CardNumber or IssuingNetwork"} , done);
    });

    it('Get available assets after bad posts (data shouldn\'t change)', function (done) {
        request(app)
            .get("/cnc/assets")
            .set('Accept', 'application/json')
            .expect('Content-Type', "application/json; charset=utf-8")
            .expect(200,[
                {"CreditDetails": { "CardNumber": 375283401434484,  "IssuingNetwork": "American Express"}}
            ], done);
    });

    it('Post another asset', function (done) {
        request(app)
            .post("/cnc/assets")
            .send({"CreditDetails": {"IssuingNetwork": "Visa", "CardNumber": 774283401654474}})
            .set('Accept', 'application/json')
            .expect('Content-Type', "application/json; charset=utf-8")
            .expect(200, {"NumAssets" : 2} , done);
    });


    it('Get available assets after second post)', function (done) {
        request(app)
            .get("/cnc/assets")
            .set('Accept', 'application/json')
            .expect('Content-Type', "application/json; charset=utf-8")
            .expect(200,[
                {"CreditDetails": { "CardNumber": 375283401434484,  "IssuingNetwork": "American Express"}},
                {"CreditDetails": {"IssuingNetwork": "Visa", "CardNumber": 774283401654474}}
            ], done);
    });

});

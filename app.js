
/*
 * Module dependencies.
 */

var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');

// Load routes.
var routes = require('./routes');

/*
 * Initialize application.
 */

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Pretty print JSON responses.
app.set('json spaces', 4);

// Set up the application's routes.
app.use('/cnc', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

/*
 * Error handlers
 */

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    message: err.message
  });
});


module.exports = app;
